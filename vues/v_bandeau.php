<?php

if(!empty($_SESSION)){
$desIdProduit = getLesIdProduitsDuPanier();
$lesProduitsDuPanier = $pdo->getLesProduitsDuTableau($desIdProduit);
$n= nbProduitsDuPanier();
}
else {
    $n = 0;
}
?>
<div id="bandeau">
    <!-- Images En-t�te -->
    <div class=" text-center">
        <img src="images/lafleur.gif" alt="Lafleur" title="Lafleur"/>
    </div>
</div>
<!--  Menu haut-->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mx-auto">
            <li class="nav-item active">
                <a class="nav-link" href="index.php?uc=accueil"">Accueil <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="index.php?uc=voirProduits&action=voirCategories">Voir le catalogue de
                    fleurs </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="index.php?uc=gererPanier&action=voirPanier">Voir son panier
                    <span class="badge badge-warning"><?php echo $n; ?></span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link disabled" href="index.php?uc=administrer&action=connexion" tabindex="-1"
                   aria-disabled="true">Administrateur</a>
            </li>
        </ul>
    </div>
</nav>

