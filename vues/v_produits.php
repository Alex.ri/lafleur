<?php
$cat = $_GET['categorie'];
switch($cat)
{
    case 'pla': {
        $cat = "Plantes";
        break;
    }
    case 'fle' : {
        $cat = "Fleurs";
        break;
    }
    case 'com' : {
        $cat = "Composition";
        break;
    }
}
?>
<div class=" container text-center voffset4">
    <h2>Catalogue des <?php echo $cat ?></h2>
    <hr>
</div>
<div class="container" id="produits">
    <div class=" row voffset2 align-items-center">
        <?php
        foreach ($lesProduits as $unProduit) {
            $id = $unProduit['id'];
            $description = $unProduit['description'];
            $prix = $unProduit['prix'];
            $image = $unProduit['image'];
            ?>
            <div class="col-12 col-sm-12 col-md-4 col-lg-4 voffset2 mx-auto">
                <div class="card shadow-lg p-3 mb-5 bg-white rounded" style="width: 18em; height: 450px;">
                    <img src="<?php echo $image ?>" class="card-img-top " alt="...">
                    <div class="card-body text-center">
                        <p class="card-text"><?php echo $description ?></p>
                        <p><?php echo $prix . " Euros" ?></p>
                        <p>
                            <a href=index.php?uc=voirProduits&categorie=<?php echo $categorie ?>&produit=<?php echo $id ?>&action=ajouterAuPanier>
                                <img src="images/mettrepanier.png" TITLE="Ajouter au panier"</li>
                            </a>
                        </p>
                    </div>
                </div>
            </div>

            <?php
        }
        ?>
    </div>
</div>
