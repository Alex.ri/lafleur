﻿<?php if ($_REQUEST['action'] === "voirPanier") { ?>
    <div class="container">
        <div class="message text-center">
            <div class="alert alert-success" role="alert">
                <h4 class="alert-heading"><u>Information</u></h4>
                <p><?php echo $message; ?></p>
                <hr>
                <p class="mb-0">Poute toute question contacter le service client.</p>
            </div>
        </div>
    </div>
<?php } elseif ($_REQUEST['action'] === "confirmerCommande") { ?>

    <div class=" container text-center voffset4">
        <h2>Commande effectuer avec succès</h2>
        <p>Nous vous remercions de votre confiance.</p>
        <p><a href="/">Cliquer ici pour revenir à la page d'accueil.<a></p>
        <hr>
    </div>

<?php } else { ?>
    <div class="toast" style="position: absolute !important; right: 12px; background-color: #959f3b73;"
         data-delay="2000" role="alert" aria-live="assertive" aria-atomic="true">
        <div class="toast-header">
            <strong class="mr-auto">Information</strong>
            <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="toast-body">
            <?php echo $message; ?>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('.toast').toast('show');
        });
    </script>

<?php } ?>