<link rel="stylesheet" type="text/css" href="util/cssGeneral.css">

<div id="myNav" class="overlay">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
    <div class="overlay-content">
        <?php
        foreach ($lesCategories as $uneCategorie) {
            $idCategorie = $uneCategorie['id'];
            $libCategorie = $uneCategorie['libelle'];
            ?>
            <a href=index.php?uc=voirProduits&categorie=<?php echo $idCategorie ?>&action=voirProduits><?php echo $libCategorie ?></a>
            <?php
        }
        ?>
    </div>
</div>
<div class="text-center">
    <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Voir catégorie</span>
</div>

<?php $_GET['action'] === "voirCategories" &&  include 'vues/v_all_produits.php'; ?>

<script>
    function openNav() {
        document.getElementById("myNav").style.width = "100%";
    }

    function closeNav() {
        document.getElementById("myNav").style.width = "0%";
    }
</script>
     
