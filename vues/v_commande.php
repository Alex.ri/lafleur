﻿<?php
$total = 0;
foreach ($lesProduitsDuPanier as $unProduit) {
    $prix = $unProduit['prix'];
    $total += $prix;
}
?>
<link rel="stylesheet" href="util/cssGeneral.css" type="text/css">


<div class="container" id="creationCommande">
    <form method="POST" action="index.php?uc=gererPanier&action=confirmerCommande">
        <h4>Coordonnées</h4>
        <h5>Addresse d'expédition</h5>
        <div class="form-row voffset4">
            <div class="form-group col-md-6">
                <label for="nom">Nom Prénom*</label>
                <input type="text" class="form-control" id="nom" name="nom" placeholder="Entrer votre Nom puis Prénom" value="<?php echo $nom ?>">
            </div>
            <div class="form-group col-md-6">
                <label for="inputZip">Email</label>
                <input type="text" class="form-control" id="mail" name="mail" placeholder="Entrer votre addresse mail" value="<?php echo $mail ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="rue">Rue*</label>
            <input type="text" class="form-control" id="rue" name="rue" placeholder="Entrer votre rue" value="<?php echo $rue ?>" >
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="cp">Code Postal</label>
                <input type="text" class="form-control" id="cp" name="cp" placeholder="Entrer votre code postal" value="<?php echo $cp ?>" >
            </div>
            <div class="form-group col-md-8">
                <label for="inputCity">Ville</label>
                <input type="text" class="form-control" id="ville" name="ville" placeholder="Entrer votre ville" value="<?php echo $ville ?>">
            </div>
        </div>

        <div class="form-group">
            <div class="form-check">
                <input class="form-check-input" type="checkbox" id="gridCheck">
                <label class="form-check-labFel" for="gridCheck">
                    Sauvegarder mes informations
                </label>
            </div>
        </div>
        <p class="text-center">
            <button type="submit" class="btn btn-dark">Payer <?php echo $total; ?>€</button>
        </p>
    </form>
</div>





