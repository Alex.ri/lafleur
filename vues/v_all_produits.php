<div class="container" id="produits">
    <div class="text-center voffset4">
    <h2>Catalogue de tout les produits</h2>
        <hr>
    </div>
    <div class=" row voffset2 align-items-center">
        <?php
        foreach ($allFleurs as $unProduit) {
            $id = $unProduit['id'];
            $description = $unProduit['description'];
            $prix = $unProduit['prix'];
            $image = $unProduit['image'];
            $categorie = $unProduit['idCategorie']
            ?>
            <div class="col-12 col-sm-12 col-md-4 col-lg-4 voffset2 mx-auto">
                <div class="card shadow-lg p-3 mb-5 bg-white rounded" style="width: 18em; height: 450px;">
                    <img src="<?php echo $image ?>" class="card-img-top " alt="...">
                    <div class="card-body text-center">
                        <p class="card-text"><?php echo $description ?></p>
                        <p><?php echo $prix . " Euros" ?></p>
                        <p>
                            <a href=index.php?uc=voirProduits&categorie=<?php echo $categorie ?>&produit=<?php echo $id ?>&action=ajouterAuPanier&type=all>
                                <img src="images/mettrepanier.png" TITLE="Ajouter au panier"</li>
                            </a>
                        </p>
                    </div>
                </div>
            </div>

            <?php
        }
        ?>
    </div>
</div>