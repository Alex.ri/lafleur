<link rel="stylesheet" href="util/cssGeneral.css" type="text/css">
<html>
<body>
<div class="container">
    <div id="carouselExampleIndicators" class="carousel slide carousel-fade" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner ">
            <div class="carousel-item active" data-interval="10000">
                <img src="images/carous/abc.jpg" class="d-block carrouss" alt="fleur1">
            </div>
            <div class="carousel-item" data-interval="10000">
                <img src="images/carous/abcd.jpg" class="d-block carrouss" alt="fleur2">
            </div>
            <div class="carousel-item" data-interval="10000">
                <img src="images/carous/abcde.jpg" class="d-block carrouss" alt="fleur3">
            </div>
            <div class="carousel-item" data-interval="10000">
                <img src="images/carous/abcdef.jpg" class="d-block carrouss" alt="fleur4">
            </div>
            <div class="carousel-item" data-interval="10000">
                <img src="images/carous/abcdefg.jpg" class="d-block carrouss" alt="fleur5">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>

<!--    TITTRE BIENVENUE-->


<div class="container">
    <div class="row">
        <svg viewBox="0 0 960 300">
            <symbol id="s-text">
                <text text-anchor="middle" x="50%" y="80%">Bienvenue</text>
            </symbol>
            <g class="g-ants">
                <use xlink:href="#s-text" class="text-copy"></use>
                <use xlink:href="#s-text" class="text-copy"></use>
                <use xlink:href="#s-text" class="text-copy"></use>
                <use xlink:href="#s-text" class="text-copy"></use>
                <use xlink:href="#s-text" class="text-copy"></use>
            </g>
        </svg>
    </div>
</div>
</div>


</body>
</html>
    
    
