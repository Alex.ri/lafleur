﻿<div class="erreur">
    <ul>
        <?php
        foreach ($msgErreurs as $erreur) {
            ?>
            <div class="container">
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>Erreur ! </strong><?php echo $erreur ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            <?php
        }
        ?>
    </ul>
</div>
