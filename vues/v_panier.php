<link rel="stylesheet" type="text/css" href="util/cssGeneral.css">
<div class="container">
<h2 class="text-center">Récapitulatif de votre Panier</h2>
<hr/>
    </div>
<?php



$total = 0;
foreach ($lesProduitsDuPanier as $unProduit) {
    $id = $unProduit['id'];
    $description = $unProduit['description'];
    $image = $unProduit['image'];
    $prix = $unProduit['prix'];

$total += $prix;

?>
<div class="container">
    <div class="text-center">
        <img src="<?php echo $image ?>" alt=image width=100 height=100/>
        </br>
        <?php
        echo $description . "($prix Euros)";
        ?>
        </br>
            <select  name='quantite'>
                <?php

                for ($i = 1; $i < 13; $i++): ?>
                    <option> <?= $i; ?></option>
                <?php endfor; ?>
            </select>
        <a href="index.php?uc=gererPanier&produit=<?php echo $id ?>&action=supprimerUnProduit"
           onclick="return confirm('Voulez-vous vraiment retirer cet article frais?');">
            <img src="images/retirerpanier.png" TITLE="Retirer du panier"></a>
    </div>
    <?php
    }
    ?>
    <br>
    <div class="text-center">

        <p>
            Soit un total de <b><?php echo $total; ?> €</b>.
        </p>
        <a href=index.php?uc=gererPanier&action=passerCommande><img src="images/commander.jpg" TITLE="Passer commande"></a>
    </div>
</div>
